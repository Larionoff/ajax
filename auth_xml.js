var elements = {
  form: document.getElementById('auth'),
  email: document.getElementById('auth__email'),
  password: document.getElementById('auth__password'),
  submit: document.getElementById('auth__submit'),
  preloader: document.getElementById('auth__preloader'),
  error: document.getElementById('auth__error'),
  profile: document.getElementById('profile'),
  avatar: document.getElementById('profile__avatar'),
  fullname: document.getElementById('profile__fullname'),
  country: document.getElementById('profile__country'),
  hobbies: document.getElementById('profile__hobbies'),
  logout: document.getElementById('profile__logout')
};

Document.prototype.get = function(tag)
{
  var tags = this.getElementsByTagName(tag);
  return tags.length>0 ? tags[0].innerHTML : null;
};

Document.prototype.getArray = function(tag,itemTag)
{
  var
    tags = this.getElementsByTagName(tag),
    container = tags.length>0 ? tags[0] : null,
    values = [];
  if (container)
  {
    var children = container.getElementsByTagName(itemTag);
    for (var i=0; i<children.length; i++) values.push(children[i].innerHTML);
  }
  return values;
};

function ajax(type,url,data,start,load)
{
  var xhr = new XMLHttpRequest();
  xhr.open(type,url,true);
  xhr.setRequestHeader('Content-Type','application/json');
  xhr.onloadstart = start;
  xhr.onload = function()
  {
    load(this.responseXML);
  };
  xhr.send(JSON.stringify(data));
}

elements.submit.onclick = function(event)
{
  ajax(elements.form.method,elements.form.action,{email: elements.email.value, password: elements.password.value},function() // onloadstart
  {
    elements.form.classList.add('loading');
  },function(xml) // onload
  {
    elements.form.classList.remove('loading');
    var xmlError = xml.getElementsByTagName('error').length>0 ? xml.getElementsByTagName('error')[0] : null;
    var message = xmlError ? xml.get('message') : null;
    if (xmlError && message)
    {
      elements.error.innerHTML = '<b>ERROR!</b><br><br>' + message + '<br><br>Error code: ' + xmlError.getAttribute('code');
      elements.form.classList.add('error');
      var close = document.createElement('span');
      close.className = 'auth__error-close';
      close.innerHTML = '&lt;&lt; Back';
      close.onclick = function()
      {
        elements.form.classList.remove('error');
        this.remove();
      };
      elements.error.appendChild(close);
      return;
    }
    var person = {
      userpic: xml.get('userpic'),
      fullname: xml.get('name') + ' ' + xml.get('lastname'),
      country: xml.get('country'),
      hobbies: xml.getArray('hobbies','hobby')
    };
    elements.form.classList.add('hidden');
    elements.profile.classList.remove('hidden');
    elements.avatar.src = person.userpic;
    elements.fullname.innerHTML = '<b>Full name</b>: ' + person.fullname;
    elements.country.innerHTML = '<b>Country</b>: ' + person.country;
    elements.hobbies.innerHTML = '<b>Hobbies</b>: ' + person.hobbies.join(', ');
  });
  event.preventDefault();
};

elements.logout.onclick = function()
{
  elements.profile.classList.add('hidden');
  elements.email.value = '';
  elements.password.value = '';
  elements.form.classList.remove('hidden');
};