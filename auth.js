var elements = {
  form: document.getElementById('auth'),
  email: document.getElementById('auth__email'),
  password: document.getElementById('auth__password'),
  submit: document.getElementById('auth__submit'),
  preloader: document.getElementById('auth__preloader'),
  error: document.getElementById('auth__error'),
  profile: document.getElementById('profile'),
  avatar: document.getElementById('profile__avatar'),
  fullname: document.getElementById('profile__fullname'),
  country: document.getElementById('profile__country'),
  hobbies: document.getElementById('profile__hobbies'),
  logout: document.getElementById('profile__logout')
};

function http_build_query(query_data)
{
  var pairs = [];
  for (var param in query_data)
  {
    pairs.push(param + '=' + query_data[param]);
  }
  return pairs.join('&');
}

function ajax(type,url,data,start,load)
{
  var xhr = new XMLHttpRequest();
  xhr.open(type,url,true);
  xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
  xhr.onloadstart = start;
  xhr.onload = function()
  {
    load(this.responseText);
  };
  xhr.send(http_build_query(data));
}

elements.submit.onclick = function(event)
{
  ajax(elements.form.method,elements.form.action,{email: encodeURIComponent(elements.email.value), password: encodeURIComponent(elements.password.value)},function() // onloadstart
  {
    elements.form.classList.add('loading');
  },function(json) // onload
  {
    elements.form.classList.remove('loading');
    var data = JSON.parse(json);
    if (data.hasOwnProperty('error'))
    {
      elements.error.innerHTML = '<b>ERROR!</b><br><br>' + data.error.message + '<br><br>Error code: ' + data.error.code;
      elements.form.classList.add('error');
      var close = document.createElement('div');
      close.className = 'auth__error-close';
      close.onclick = function()
      {
        elements.form.classList.remove('error');
        this.remove();
      };
      elements.error.appendChild(close);
      return;
    }
    elements.form.classList.add('hidden');
    elements.profile.classList.remove('hidden');
    elements.avatar.src = data.userpic;
    elements.fullname.innerHTML = '<b>Full name</b>: ' + data.name + ' ' + data.lastname;
    elements.country.innerHTML = '<b>Country</b>: ' + data.country;
    elements.hobbies.innerHTML = '<b>Hobbies</b>: ' + data.hobbies.join(', ');
  });
  event.preventDefault();
};

elements.logout.onclick = function()
{
  elements.profile.classList.add('hidden');
  elements.email.value = '';
  elements.password.value = '';
  elements.form.classList.remove('hidden');
};